import { Component, OnInit } from '@angular/core';
import {Chaussure} from '../../models/chaussure';
import {ActivatedRoute, Router} from '@angular/router';
import {ChaussureService} from '../../services/chaussure.service';

@Component({
  selector: 'app-edit-chaussure',
  templateUrl: './edit-chaussure.component.html',
  styleUrls: ['./edit-chaussure.component.css']
})
export class EditChaussureComponent implements OnInit {
  chaussureForm: Chaussure;
  isLoading: boolean;
  marqueDisponible: string[];
  typeChaussureDisponible: string[];

  constructor(private activatedRoute: ActivatedRoute,
              private chaussureService: ChaussureService, private router: Router) { }

  ngOnInit() {
    this.isLoading = true;
    this.marqueDisponible = this.chaussureService.marqueDisponible;
    this.typeChaussureDisponible = this.chaussureService.typeChaussureDispo;
    this.chaussureService.getOneById(+this.activatedRoute.snapshot.paramMap.get('id'))
      .subscribe(data => {
        this.chaussureForm = data;
        this.isLoading = false;
      });
  }

  editChaussure() {
    this.chaussureService.editChaussure(this.chaussureForm).subscribe((data)=> {
      this.router.navigate(['/dashboard']);
    });
  }

}
