import { Component, OnInit } from '@angular/core';
import {Chaussure} from '../../models/chaussure';
import {ChaussureService} from '../../services/chaussure.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-chaussure',
  templateUrl: './add-chaussure.component.html',
  styleUrls: ['./add-chaussure.component.css']
})
export class AddChaussureComponent implements OnInit {
  chaussureForm: Chaussure;
  marqueDisponible: string[];
  typeChaussureDisponible: string[];

  constructor(private chaussureService: ChaussureService, private router: Router) { }

  ngOnInit() {
    this.marqueDisponible = this.chaussureService.marqueDisponible;
    this.typeChaussureDisponible = this.chaussureService.typeChaussureDispo;

    this.chaussureForm = new Chaussure();
  }

  addChaussure() {
    this.chaussureService.add(this.chaussureForm).subscribe(data => {
      this.router.navigate(['/dashboard']);
    });

  }

}
