import { Component, OnInit } from '@angular/core';
import {Chaussure} from '../../models/chaussure';
import {ChaussureService} from '../../services/chaussure.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  chaussures: Chaussure[];
  isLoading: boolean;
  constructor(private chaussureService: ChaussureService) { }

  ngOnInit() {
    this.isLoading = true;
    this.chaussureService.getAll().subscribe(data => {
      this.chaussures = data;
      this.isLoading = false;
    });
  }

  deleteChaussure(chaussure: Chaussure) {
    this.isLoading = true;
    this.chaussureService.remove(chaussure).subscribe(data => {
      this.chaussureService.getAll().subscribe(allChaussures => {
        this.chaussures = allChaussures;
        this.isLoading = false;
      });
    });
  }

}
