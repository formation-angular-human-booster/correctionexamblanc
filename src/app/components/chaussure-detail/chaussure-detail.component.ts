import { Component, OnInit } from '@angular/core';
import {Chaussure} from '../../models/chaussure';
import {ChaussureService} from '../../services/chaussure.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-chaussure-detail',
  templateUrl: './chaussure-detail.component.html',
  styleUrls: ['./chaussure-detail.component.css']
})
export class ChaussureDetailComponent implements OnInit {
  chaussure: Chaussure;
  isLoading: boolean;
  constructor(private chaussureService: ChaussureService,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.isLoading = true;
    this.chaussureService.getOneById(+this.activatedRoute.snapshot.paramMap.get('id'))
      .subscribe(data => {
        this.chaussure = data;
        this.isLoading = false;
      });
  }

}
