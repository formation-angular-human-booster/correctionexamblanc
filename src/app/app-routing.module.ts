import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {AddChaussureComponent} from './components/add-chaussure/add-chaussure.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {EditChaussureComponent} from './components/edit-chaussure/edit-chaussure.component';
import {ChaussureDetailComponent} from './components/chaussure-detail/chaussure-detail.component';


const routes: Routes = [
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'add', component: AddChaussureComponent},
  {path: 'edit/:id', component: EditChaussureComponent},
  {path: 'chaussure/:id', component: ChaussureDetailComponent},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
