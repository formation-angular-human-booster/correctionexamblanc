import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './components/menu/menu.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddChaussureComponent } from './components/add-chaussure/add-chaussure.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { EditChaussureComponent } from './components/edit-chaussure/edit-chaussure.component';
import { ChaussureDetailComponent } from './components/chaussure-detail/chaussure-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    AddChaussureComponent,
    DashboardComponent,
    NotFoundComponent,
    EditChaussureComponent,
    ChaussureDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
