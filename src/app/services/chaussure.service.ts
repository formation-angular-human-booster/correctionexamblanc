import { Injectable } from '@angular/core';
import {Chaussure} from '../models/chaussure';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class ChaussureService {
  marqueDisponible = ['Adidas', 'Nike', 'Puma'];
  typeChaussureDispo = ['Sport', 'Ville', 'Football'];
  urlApi = 'http://localhost:3000/chaussure';
  constructor(private httpClient: HttpClient) { }

  add(chaussure: Chaussure): Observable<Chaussure> {
    chaussure.dateEntreStock = new Date();
    return this.httpClient.post<Chaussure>(this.urlApi, chaussure).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  getAll(): Observable<Chaussure[]> {
    return this.httpClient.get<Chaussure[]>(this.urlApi).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  getOneById(id: number): Observable<Chaussure> {
    return this.httpClient.get<Chaussure>(this.urlApi + '/' + id).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  remove(chaussure: Chaussure): Observable<Chaussure> {
    return this.httpClient.delete<Chaussure>(this.urlApi + '/' + chaussure.id).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
  editChaussure(chaussure: Chaussure): Observable<Chaussure> {
    return this.httpClient.put<Chaussure>(this.urlApi + '/' + chaussure.id, chaussure).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  handleError(error) {
    let errorMessage = '';
    if ( error.error instanceof ErrorEvent ) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }




}
