
import {browser, by, element, logging} from 'protractor';
import {ChaussurePage} from './chaussures.po';

describe('Test de nos chaussures', () => {
  let page: ChaussurePage;
  let nbLineInit: number;

  beforeEach(() => {
    page = new ChaussurePage();
    browser.get('/dashboard');
  });

  it('Je compte le nombre de ligne dans le tableau et je test la navigation vers mon url d\'ajout', () => {
    element.all(by.css('.lineChaussure')).then(totalRows => {
      this.nbLineInit = totalRows.length;
    });
    element.all(by.css('#buttonAddChaussure')).first().click();
    expect(browser.driver.getCurrentUrl()).toContain('/add');
  });

  it('Je remplis le formulaire, et je le valide et je suis revenu sur la page d\'accueil', () => {
    browser.get('/add');
    page.completeForm();
    element.all(by.id('submitFormChaussure')).click();
    page.sleep();
    expect(browser.driver.getCurrentUrl()).toContain('/dashboard');
  });


  it('Notre tableau contient une ligne de plus !', () => {
    element.all(by.css('.lineChaussure')).then(totalRows => {
      this.nbLineInit += 1;
      expect(totalRows.length).toEqual(this.nbLineInit);
    });
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });


});
